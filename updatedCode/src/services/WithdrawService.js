const config = require('../config');
const db = require('../database/database');
const { minEarnText, defaultErrorMessage } = require('../utils/constants');

// Returns boolean
function canUserWithdraw(type, countryCode) {
  return (
    config.withdraw.acceptedVendorTypes.includes(type) &&
    config.withdraw.acceptedCountryCode.includes(countryCode)
  );
}

async function onsiteGiftcardWithdraw(data, gainid, feedback, socket) {
  if (!data) return feedback(`An unknown error occurred`);

  let type = data.type,
    countryCode = data.countryCode || 'WW',
    coinAmount = parseInt(data.coinAmount);

  if (!canUserWithdraw(type, countryCode))
    return feedback(`An error occurred. Please try refreshing.`);

  if (isNaN(coinAmount) || !coinAmount) return feedback(`Please select an amount!`);

  try {
    // check standing
    const accountStanding = await db.getAccountStanding(gainid);
    if (accountStanding === null) return feedback(defaultErrorMessage);

    if (accountStanding.banned || accountStanding.frozen) {
      return feedback(
        `You are currently banned from withdrawing, please contact staff if you believe this is a mistake.`
      );
    }

    // check email verification
    const isEmailVerified = await db.isDefaultUserEmailVerified(gainid);
    if (isEmailVerified === null) return feedback(defaultErrorMessage);

    if (!isEmailVerified)
      return feedback(`You must verify your E-mail address before requesting a withdrawal!`);

    // check balance
    const balance = await db.getBalanceByGainId(gainid);
    if (balance === null) {
      return feedback(defaultErrorMessage);
    }

    if (balance < coinAmount) {
      return feedback(`You don't have enough balance!`);
    }

    // check if withdraw criteria met
    const earnedEnoughBool = await db.earnedEnoughToWithdraw(gainid);
    if (earnedEnoughBool === null) {
      return feedback(`An error occurred, please try again.`);
    }
    if (!earnedEnoughBool) {
      return feedback(minEarnText);
    }

    const result = await GiftcardService.isGiftCardInStock(type, coinAmount, countryCode);
    // if there's an error, it can't be determined if the card is in stock or not
    if (result === null) return feedback(defaultErrorMessage);
    // if it's on stock continue as usual
    if (typeof result === 'boolean')
      return notVerified(result, gainid, coinAmount, type, countryCode, socket);

    // otherwise, tell the user to pick another card.
    return feedback(`This card is currently out of stock. Please choose another.`);
  } catch (error) {
    console.error(error);
    return feedback(defaultErrorMessage);
  }
}

async function notVerified(outOfStock, gainid, coinAmount, type, countryCode, socket) {
  await db.updateBalanceByGainId(gainid, coinAmount * -1);
  await db.insertPendingSiteGiftCardWithdraw(
    gainid,
    coinAmount,
    type,
    countryCode,
    utils.getIsoString(),
    null
  );

  socket.emit('withdrawalPending', {
    coins: coinAmount,
  });

  Notifications.storeNotification(
    gainid,
    'Info',
    'pendingwithdrawal',
    `Your ${type} Gift Card withdrawal worth ${coinAmount} coins is pending.`
  );

  if (outOfStock) {
    feedback(
      `Success!<br>This card is currently out of stock. A staff member will approve your withdrawal when it is restocked.`
    );
  } else {
    feedback(
      `Success!<br>Since you are not verified, a staff member has been notified and will review your withdrawal shortly! Check your Profile page to view your redemption code after the withdrawal has been approved.<br><br>Have an opinion on our site? Share it by <a href="https://trustpilot.com/evaluate/freecash.com" target="_blank">writing a Trustpilot review</a>!`
    );
  }

  emitBalance(gainid);
}

async function giftcardWithdrawForVerifiedUser(
  userId,
  gainId,
  giftType,
  coinPrice,
  countryCode,
  feedback
) {
  // get user
  const user = db.getUserByUserId(userId);

  // check if verified
  if (user?.verified) {
    // get the giftcard
    try {
      const giftcard = await GiftcardService.getGiftcard(giftType, coinPrice, countryCode);

      // insert withdrawal into DB
      await db.insertSiteGiftCardWithdrawal(
        gainId,
        coinPrice,
        giftcard.code,
        giftcard.type,
        countryCode,
        new Date(),
        null
      );

      socket.emit('withdrawalApproved', {
        coins: coinPrice,
      });

      const successMessage = `Your ${type} Gift Card withdrawal worth ${coinPrice} coins is approved.`;

      Notifications.storeNotification(gainId, 'Info', 'approvedwithdrawal', successMessage);

      feedback(successMessage);
    } catch (error) {
      if (error instanceof GiftCardUnavailableError) return feedback('Giftcard is unavailable.');

      console.log(error);
      feedback(defaultErrorMessage);
    }
  } else {
    feedback('User is not verified. Approval by moderator required for withdrawal.');
  }
}

module.exports = { onsiteGiftcardWithdraw, giftcardWithdrawForVerifiedUser };
