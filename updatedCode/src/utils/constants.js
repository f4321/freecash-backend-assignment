import config from '../config';

const queryErrorText = 'Error!';
const defaultErrorMessage = `An error occurred. Please try refreshing.`;

const minEarnText = `You must earn at least ${config.withdraw.minEarnedToWithdraw} coins ($${(
  config.withdraw.minEarnedToWithdraw / 1000
).toFixed(
  2
)}) through the offer walls before withdrawing.<br>This is to prevent abuse of the site bonuses. Please contact staff with any questions.`;

export { queryErrorText, minEarnText, defaultErrorMessage };
