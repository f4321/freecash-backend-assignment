const { onsiteGiftcardWithdraw } = require('../services/WithdrawService');

module.exports = (socket) => {
  let socketuser = socket.request.user;
  // Socketuser is false if the user is not logged in
  if (socketuser.logged_in == false) socketuser = false;

  function feedback(feedback, feedbackType) {
    socket.emit('withdrawFeedback', feedback, feedbackType);
  }

  function withdrawSocketHandler(data) {
    if (!socketuser) return feedback(`Please login to withdraw!`);
    return onsiteGiftcardWithdraw(data, socketuser.gainid, feedback, socket);
  }

  socket.on('onsiteGiftcardWithdraw', withdrawSocketHandler);
};
