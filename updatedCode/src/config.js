const { DB_HOST, DB_PORT, DB_USERNAME, DB_PASSWORD, DN_NAME, DB_LIMIT } = process.env;

const config = {
  withdraw: {
    minEarnedToWithdraw: 200,
    acceptedVendorTypes: ['Fortnite', 'Visa', 'Amazon', 'Steam', 'Roblox'],
    acceptedCountryCode: ['US', 'UK', 'CA', 'DE', 'FR', 'AU', 'WW'],
  },
  database: {
    connectionLimit: DB_LIMIT,
    password: DB_PASSWORD,
    user: DB_USERNAME,
    database: DN_NAME,
    host: DB_HOST,
    port: DB_PORT,
  },
  giftcard: {
    maxGiftCardValue: 99,
    acceptedVendors: ['Amazon'],
    acceptedCountryCode: ['US', 'UK'],
  },
};

module.exports = config;
