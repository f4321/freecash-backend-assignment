const mysql = require('mysql');
const config = require('../config');
const assert = require('better-assert');
const Promise = require('bluebird');
const {
  getAccountStanding,
  isDefaultUserEmailVerified,
  getBalanceByGainId,
  earnedEnoughToWithdraw,
  updateBalanceByGainId,
  insertNewBalance,
  insertPendingSiteGiftCardWithdraw,
  insertSiteGiftCardWithdrawal,
} = require('./queries');
const connection = mysql.createConnection(config.database);

// node native promisify
const query = util.promisify(connection.query).bind(connection);

const db = {
  getAccountStanding: async function (gainid) {
    const result = await query(getAccountStanding, [
      gainid,
      gainid,
      gainid,
      gainid,
      gainid,
      gainid,
    ]);
    if (!result) return null;

    let accountStanding = {
      banned: false,
      frozen: false,
      muted: false,
      countryBanned: false,
      deleted: false,
    };

    if (result[0].length) accountStanding.banned = true;
    if (result[1].length) accountStanding.frozen = true;
    if (result[2].length) accountStanding.muted = true;
    if (result[3].length) accountStanding.countryBanned = true;
    if (result[4][0].deleted) accountStanding.deleted = true;

    return accountStanding;
  },
  isDefaultUserEmailVerified: async function (gainid) {
    const result = await query(isDefaultUserEmailVerified, [gainid]);
    if (!result) return null;

    if ((result && result[0] && result[0].email_confirmed) || !result[0]) return true;

    return false;
  },
  getBalanceByGainId: async function (gainid) {
    const result = await query(getBalanceByGainId, [gainid]);
    if (!result) return null;

    assert(result.length === 1);

    return result[0].balance;
  },
  earnedEnoughToWithdraw: async function (gainid) {
    const result = await query(earnedEnoughToWithdraw, [gainid, gainid, gainid]);
    if (!result) return null;

    if (result[0].coins && result[0].coins >= config.withdraw.minEarnedToWithdraw) return true;

    return false;
  },
  updateBalanceByGainId: async function (gainid, amount) {
    let result = await query(updateBalanceByGainId, [amount, gainid]);
    if (!result) return null;

    assert(result.affectedRows === 1);

    result = await query(insertNewBalance, [gainid, amount, gainid]);
    if (!result) return null;

    assert(result.affectedRows === 1);
  },
  insertPendingSiteGiftCardWithdraw: async function (
    gainid,
    coinAmount,
    cardType,
    countryCode,
    date,
    warningMessage
  ) {
    let result = await query(insertPendingSiteGiftCardWithdraw, [
      gainid,
      date,
      warningMessage,
      coinAmount,
      cardType,
      date,
      countryCode,
    ]);
    if (!result) return null;

    assert(result.length === 2);
    return result;
  },
  insertSiteGiftCardWithdrawal: async function (
    gainid,
    coinAmount,
    cardCode,
    cardType,
    countryCode,
    date,
    approver
  ) {
    const result = await query(insertSiteGiftCardWithdrawal, [
      gainid,
      date,
      approver,
      coinAmount,
      cardCode,
      cardType,
      date,
      countryCode,
    ]);
    if (!result) return null;

    assert(result.length === 2);
    return result;
  },
};

module.exports = Promise.promisifyAll(db) || db;
