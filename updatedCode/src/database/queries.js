const getAccountStanding = `
SELECT * FROM banned WHERE gainid = ?;
SELECT * FROM frozen WHERE gainid = ?;
SELECT * FROM muted WHERE gainid = ?;
SELECT * FROM countrybanned WHERE gainid = ?;
SELECT deleted FROM users WHERE gainid = ?;
`;

const isDefaultUserEmailVerified = `
SELECT email_confirmed FROM defaultusers WHERE gainid = ?`;

const getBalanceByGainId = `SELECT balance FROM users WHERE gainid = ?`;

const earnedEnoughToWithdraw = `
SELECT SUM(coins) AS coins FROM (
  SELECT SUM(coins) AS coins FROM surveys WHERE gainid = ?
    UNION ALL
  SELECT SUM(coins) AS coins FROM videos WHERE gainid = ?
    UNION ALL
  SELECT SUM(coins) AS coins FROM refearnings WHERE gainid = ?
) AS f
`;

const updateBalanceByGainId = `
UPDATE users SET balance = balance + ? WHERE gainid = ?
`;

const insertNewBalance = `
INSERT INTO balance_movements (gainid, amount, new_balance) VALUES (?, ?, (SELECT balance FROM users WHERE gainid = ?));`;

const insertPendingSiteGiftCardWithdraw = `
      INSERT INTO pendingwithdraw (gainid, date, warning_message) VALUES (?, ?, ?);
      INSERT INTO pending_site_gift_card_withdraw (releaseid, coinamount, card_type, date, country_code) VALUES
      (LAST_INSERT_ID(), ?, ?, ?, ?);
    `;

const insertSiteGiftCardWithdrawal = `
INSERT INTO withdraw (gainid, date, approver) VALUES (?, ?, ?);
INSERT INTO site_gift_card_withdraw (withdrawid, coinamount, card_code, card_type, date, country_code) VALUES
(LAST_INSERT_ID(), ?, ?, ?, ?, ?)
`;

export {
  getAccountStanding,
  isDefaultUserEmailVerified,
  getBalanceByGainId,
  earnedEnoughToWithdraw,
  updateBalanceByGainId,
  insertNewBalance,
  insertPendingSiteGiftCardWithdraw,
  insertSiteGiftCardWithdrawal,
};
